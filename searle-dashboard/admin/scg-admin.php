<?php
/**
 * Admin facing functionality of the plugin
 * 
 * @link https://www.searlecreative.com
 * @since 1.0.0
 * @package Searle Dashboard
 * @subpackage Searle Dashboard/admin
 */

class SearleAdmin {
  // Enqueue admin facing styles
  public function enqueue_styles() {
    
  }
  
  // Enqueue admin facing scripts
  public function enqueue_scripts() {
    
  }
}