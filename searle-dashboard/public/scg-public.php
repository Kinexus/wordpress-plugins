<?php
/**
 * Public facing functionality of the plugin
 * 
 * @link https://www.searlecreative.com
 * @since 1.0.0
 * @package Searle Dashboard
 * @subpackage Searle Dashboard/public
 */

class SearlePublic {
  // Enqueue public facing styles
  public function enqueue_styles() {
    
  }
  
  // Enqueue public facing scripts
  public function enqueue_scripts() {
    
  }
}