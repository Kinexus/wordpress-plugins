<?php
/**
 * Fired on plugin activation
 * 
 * @link https://www.searlecreative.com
 * @since 1.0.0
 * @package Searle Dashboard
 * @subpackage Searle Dashboard/includes
 */
 
class SearleActivator {
  public static function activate() {
    ?>
      <script>
        console.log('Searle Dashboard activated.');
      </script>
    <?php
  }
}