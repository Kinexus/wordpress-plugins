<?php
/**
 * Fired on plugin deactivation
 * 
 * @link https://www.searlecreative.com
 * @since 1.0.0
 * @package Searle Dashboard
 * @subpackage Searle Dashboard/includes
 */
 
class SearleDeactivator {
  public static function deactivate() {
    ?>
      <script>
        console.log('Searle Dashboard deactivated.');
      </script>
    <?php
  }
}