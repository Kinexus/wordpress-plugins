<?php

class SearleDashboardSettingsPage {
  private $options;
  
  /* Init */
  public function __construct() {
    add_action('admin_menu', array($this, 'add_plugin_page'));
    add_action('admin_init', array($this, 'page_init'));
  }
  
  /* Create Options Page */
  public function add_plugin_page() {
    add_menu_page(
      'Searle Dashboard',
      'Searle Dashboard',
      'manage_options',
      'scg-dashboard',
    )
  }
}