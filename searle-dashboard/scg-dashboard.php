<?php 
/**
 * @wordpress-plugin
 * Plugin Name: Searle Dashboard
 * Version: 1.0.0
 * Plugin URI: https://searlecreative.com/
 * Description: Brings all in house plugins into one place.
 * Author: Searle Creative, Josh Vance
 * Author URI: https://www.searlecreative.com/
 * Requires at least: 5.0.3
 * Tested up to: 5.0.3
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package Searle Dashboard
 * @author Josh Vance
 * @since 1.0.0
 */

// Abort if this file is called directly.
if (!defined('WPINC')) { die; }

// Define plugin version
define('PLUGIN_NAME_VERSION', '1.0.0');

require_once plugin_dir_path(__FILE__) . 'includes/scg-analytics.php';          // Reliable Google Analytics tracking
require_once plugin_dir_path(__FILE__) . 'includes/scg-dashboard.php';          // Core Plugin Class
require_once plugin_dir_path(__FILE__) . 'includes/scg-development.php';        // Functions useful during development 
require_once plugin_dir_path(__FILE__) . 'includes/scg-functions.php';          // Basic functions used for most sites
require_once plugin_dir_path(__FILE__) . 'includes/scg-loader.php';             // Loader
require_once plugin_dir_path(__FILE__) . 'includes/scg-port.php';               // Import/Export settings
require_once plugin_dir_path(__FILE__) . 'includes/scg-reporter.php';           // Reporter to our management app
require_once plugin_dir_path(__FILE__) . 'includes/scg-settings.php';           // Settings Page
require_once plugin_dir_path(__FILE__) . 'admin/scg-admin.php';                 // Admin specific functions
require_once plugin_dir_path(__FILE__) . 'public/scg-public.php';               // Public specific functions

// Called on plugin activation
function activate_searle_dashboard() {
  require_once plugin_dir_path(__FILE__) . 'includes/scg-activate.php');
  SearleActivator::activate();
}

// Called on plugin deactivation
function deactivate_searle_dashboard() {
  require_once plugin_dir_path(__FILE__) . 'includes/scg-deactivate.php');
  SearleDeactivator::deactivate();
}

// Registering activation/deactivation hooks
register_activation_hook(__FILE__, 'activate_searle_dashboard');
register_deactivation_hook(__FILE__, 'deactivate_searle_dashboard');
