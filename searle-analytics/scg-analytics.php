<?php
/**
 * Plugin Name: Searle Analytics
 * Version: 1.0.0
 * Plugin URI: https://searlecreative.com/
 * Description: Adds Google Analytics code into site using only the Analytics tracking code.
 * Author: Searle Creative, Josh Vance
 * Author URI: https://www.searlecreative.com/
 * Requires at least: 5.0
 * Tested up to: 5.0
 *
 * Text Domain: scg-analytics
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Josh Vance
 * @since 1.0.0
 */
 
 define( 'PLUGIN_NAME_VERSION', '1.0.0' );
 
if(!defined('ABSPATH')) exit;

require_once('scg-functions.php');

// Actions
add_action('admin_menu', 'scg_analytics_admin_page');
add_action( 'admin_enqueue_scripts', 'scg_analytics_scripts' );
add_action('admin_head', 'scg_analytics_js_code');
add_action('wp_ajax_scg_analytics_ajax', 'scg_analytics_ajax_callback');

function scg_analytics_admin_page() {
  add_menu_page('Searle Analytics', 'SCG Analytics', 'manage_options', 'scg-analytics', 'scg_analytics_settings', 'dashicons-chart-line', 99);
}

function scg_analytics_settings() {
  $settings_title = 'Searle Analytics';
  $settings_description = 'Input your Google Analytics tracking code.';
  $settings_submit_text = 'Save';
  $success_message = '<p id="scg-analytics-success" style="display:none;color:#3cbd41;transition:all 0.2s ease-in-out;">UA code updated successfully!</p>';
  $failure_message = '<p id="scg-analytics-error" style="display:none;color:#bd3c3c;transition:all 0.2s ease-in-out;">Error: UA code failed to update. Verify the code is in the proper format and try again.</p>';
  
  // Register settings with WP
  register_setting('scg-analytics-tracking-code', 'scg-analytics-code');
  
  // Render settings page
  $settings_page = "";
  $settings_page .= "<div class='wrap'>";
  $settings_page .= " <h1>" . $settings_title . "</h1>";
  $settings_page .= " <h4>" . $settings_description . "</h4>";
  $settings_page .= " <form method='post' enctype='multipart/form-data' name='scg-analytics-code'>";
  $settings_page .= "  <input type='text' id='scg-ua-code' name='scg-analytics-code' maxlength='16' pattern='^ua-\d{4,9}-\d{1,4}$' value='" . get_option('scg-analytics-code') . "'/>";
  $settings_page .= "  <input type='submit' id='scg-submit-ua-code' name='scg-analytics-save' value='" . $settings_submit_text . "'>";
  $settings_page .= " </form>";
  $settings_page .= $success_message;
  $settings_page .= $failure_message;
  $settings_page .= "</div>";
  
  echo $settings_page;
}

function scg_analytics_scripts() {
  //wp_enqueue_script('scgAnalytics', plugins_url('scg-analytics.js', __FILE__), true);
  wp_enqueue_script('jq', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', true);
}

function isAnalytics($str){
  if(preg_match('/^ua-\d{4,9}-\d{1,4}$/i', strval($str))){
    return true;
  } else {
    return false;
  }
}

function scg_analytics_ajax_callback() {
	$uaCode = $_POST['uaCode'];
  if (isset($_POST['uaCode'])) {
    if(isAnalytics($uaCode) == true){
      if(!get_option('scg-analytics-code')){ 
        add_option('scg-analytics-code');
      }
      update_option('scg-analytics-code', $_POST['uaCode']);
    }
	die();
  }
}

function scg_analytics_js_code() {
  ?>
    <script>
      jQuery(document).ready(function() {
        function isAnalytics(str){
          return (/^ua-\d{4,9}-\d{1,4}$/i).test(str.toString());
        }
        jQuery('#scg-submit-ua-code').click(function() {
          var uaCode = jQuery('#scg-ua-code').val(); 

          var data = { 
            'action': 'scg_analytics_ajax',  
            'uaCode': uaCode
          };
          
          if(isAnalytics(uaCode) == true){
            jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(response) {
              console.log("UA code updated (" + uaCode + ")");
              jQuery('#scg-analytics-error').css('display', 'none');
              jQuery('#scg-analytics-success').css('display', 'block');
            }); 
          } else {
            console.log("UA code invalid (" + uaCode + ")"); 
            jQuery('#scg-analytics-success').css('display', 'none');
            jQuery('#scg-analytics-error').css('display', 'block');
          }
          
          return false; 
        });
      });
    </script>
  <?php
}
