<?php 

add_action('wp_head', 'scg_analytics_hook');
 
function scg_analytics_hook() {
  $scg_analytics_code = get_option('scg-analytics-code');
  if($scg_analytics_code){
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $scg_analytics_code; ?>"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', '<?php echo $scg_analytics_code; ?>');
    </script>
    <?
  } else {
    ?>
    <script>
      console.log("Google Analytics tracking code missing!");
    </script>
    <?php
  }
}
