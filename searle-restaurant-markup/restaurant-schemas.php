<?php
/*
 * Plugin Name: Searle Restaurant Schemas
 * Version: 1.0.1
 * Plugin URI: http://searlecreative.com/
 * Description: a plugin to add structured data to the restaraunt listings
 * Author: Searle Creative, Joe Howard, Josh Vance
 * Author URI: http://www.searlecreative.com/
 * Requires at least: 4.1
 * Tested up to: 4.4
 *
 * Text Domain: searle-restaurant-schemas
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Joe Howard, Josh Vance
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Define the plugin directory
define('SCHEMA_DIR', ABSPATH.'wp-content/plugins/restaurant-schemas/');

require_once( 'restaurant-schemas-functions.php' );



add_action('admin_menu', 'sc_restaurant_schemas_page');

function sc_restaurant_schemas_page() {
	add_management_page('Searle Restaurant Schemas', 'Restaurant Schemas', 'edit_theme_options', 'restaurant-schemas', 'sc_restaurant_schemas_functions');
}

function sc_restaurant_schemas_functions() {
	global $wpdb, $post, $options;

	?>

	<div class="wrap">
		<h2>Searle Restaurant Schemas Options</h2>
		<p>Nothing here yet!</p>
	</div>

<?php

}
