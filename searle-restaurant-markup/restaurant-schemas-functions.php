<?php


add_action('wp_head', 'sc_restaurant_schema_addon');

function sc_restaurant_schema_addon() {
	global $post;
	$postID = $post->ID;

	$domain = str_replace('www.', '', $_SERVER['HTTP_HOST']);
	$siteURL = str_replace('.com', '', $domain);

	$oxnardDomains = array(
		'visitoxnard',
		'seotestoxnard.wpengine',
		);
	$catalinaDomains = array(
		'catav2.wpengine',
		'catalinachamber',
		'catamerge'
		);
	$lompocDomains = array(
		'explorelompoc',
		'testlompoc.wpengine'
		);

	if (in_array($siteURL, $oxnardDomains)) {
		$siteURL = 'oxnard';
	} elseif (in_array($siteURL, $lompocDomains)) {
		$siteURL = 'lompoc';
	} elseif (in_array($siteURL, $catalinaDomains)) {
		$siteURL = 'catalina';
	}

	switch ( $siteURL ) {
		case 'oxnard':
			$catID 									= '346';
			$postType 							= 'listing';
			$listingType 						= 'listingtype';
			$schemaStreetAddress 		= 'business_address_1';
			$schemaAddressLocality 	= 'business_city';
			$schemaAddressRegion 		= 'business_state';
			$schemaPostalCode 			= 'business_zip';
			$schemaLatitude 				= 'geo_lat';
			$schemaLongitude 				= 'geo_long';
			$schemaCuisine 					= ' ';
			$schemaURL 							= 'business_url';
			$schemaPhone 						= 'business_phone';
			break;

			case 'lompoc':
			$catID 									= '23';
			$postType 							= 'directory';
			$listingType 						= 'directorytype';
			$schemaStreetAddress 		= 'dir_business_address_1';
			$schemaAddressLocality 	= 'dir_business_city';
			$schemaAddressRegion 		= 'dir_business_state';
			$schemaPostalCode 			= 'dir_business_zip';
			$schemaLatitude 				= 'geo_lat';
			$schemaLongitude 				= 'geo_long';
			$schemaCuisine 					= 'dir_business_cuisine';
			$schemaURL 							= 'dir_business_url';
			$schemaPhone 						= 'dir_business_phone';
			break;

		case 'catalina':
			$catID 									= '55';
			$postType 							= 'organizations';
			$listingType 						= 'orgcat';
			$schemaStreetAddress 		= 'scrm_org_address_1';
			$schemaAddressLocality 	= 'scrm_org_city';
			$schemaAddressRegion 		= 'scrm_org_state';
			$schemaPostalCode 			= 'scrm_org_postal';
			$schemaLatitude 				= 'geo_lat';
			$schemaLongitude 				= 'geo_long';
			$schemaCuisine 					= ' ';
			$schemaURL 							= 'scrm_org_url';
			$schemaPhone 						= 'scrm_org_phone';
			break;
	}

	if (get_post_type() == $postType) {
		$postCats = array();
		$postImage = '';

		$categories = get_the_terms($postID, $listingType, $catID);

		foreach ($categories as $category) {
			$postCats[] = $category->term_id;
		}
		if (in_array($catID, $postCats)) {

			// Get the thumbnail url
			if (has_post_thumbnail($postID)) {
				$image_URL = wp_get_attachment_image_src( get_post_thumbnail_id( $postID ), 'full' );
				$image_URL = $image_URL[0];
			} elseif (get_post_meta($postID, 'venue_image1', true) != '') {
				$image_URL = get_post_meta($postID, 'venue_image1', true);
			} elseif (get_post_meta($postID, 'lompoc_workimage', true) != '') {
				$slide1 = get_post_meta($postID, 'lompoc_workimage', true);
				$image_URL = wp_get_attachment_url( $slide1 );
			} else {
				$image_URL = ' ';
			}
			?>

			<script type="application/ld+json">
			  {
			    "@context": "http://schema.org",
			    "@type": "Restaurant",
			    "image": "<?php echo $image_URL; ?>",
			    "@id": "<?php echo get_permalink(); ?>",
			    "name": "<?php echo get_the_title(); ?>",
			    "address": {
			      "@type": "PostalAddress",
			      "streetAddress": "<?php echo get_post_meta($postID, $schemaStreetAddress, true); ?>",
			      "addressLocality": "<?php echo get_post_meta($postID, $schemaAddressLocality, true); ?>",
			      "addressRegion": "<?php echo get_post_meta($postID, $schemaAddressRegion, true); ?>",
			      "postalCode": "<?php echo get_post_meta($postID, $schemaPostalCode, true); ?>",
			      "addressCountry": "US"
			    },
			    "geo": {
			      "@type": "GeoCoordinates",
			      "latitude": "<?php echo get_post_meta($postID, $schemaLatitude, true); ?>",
			      "longitude": "<?php echo get_post_meta($postID, $schemaLongitude, true); ?>"
			    },
					"servesCuisine": "<?php echo get_post_meta($postID, $schemaCuisine, true); ?>",
			    "url": "<?php echo get_post_meta($postID, $schemaURL, true); ?>",
			    "telephone": "<?php echo get_post_meta($postID, $schemaPhone, true); ?>"
			  }
			</script>;

			<?php
		}
	}
}
